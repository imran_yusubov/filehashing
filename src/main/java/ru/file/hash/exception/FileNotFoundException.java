package ru.file.hash.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 21442332423L;

    public FileNotFoundException(){
        super("Файлы с данным хэшем не найдены.");
    }
}
