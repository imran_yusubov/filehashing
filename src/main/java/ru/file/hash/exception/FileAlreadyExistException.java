package ru.file.hash.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CREATED)
public class FileAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 42345645L;

    public FileAlreadyExistException(){
        super("File already exist.");
    }
}
