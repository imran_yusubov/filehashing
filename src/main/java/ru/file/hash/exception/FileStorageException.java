package ru.file.hash.exception;

public class FileStorageException extends RuntimeException {

    private static final long serialVersionUID = 21442435323423L;

    public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
