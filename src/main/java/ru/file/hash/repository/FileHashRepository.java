package ru.file.hash.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.file.hash.model.FileHash;
import java.util.List;

@Repository
public interface FileHashRepository extends JpaRepository<FileHash, Long> {

    List<FileHash> findByMd5OrSha256(String md5,String sha256);

    List<FileHash> deleteByMd5OrSha256(String md5, String sha256);
}
