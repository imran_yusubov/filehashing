package ru.file.hash.service;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ru.file.hash.exception.FileNotFoundException;
import ru.file.hash.exception.FileStorageException;
import ru.file.hash.model.FileHash;
import ru.file.hash.properity.FileStorageProperties;
import ru.file.hash.repository.FileHashRepository;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
@Service
public class FileServiceImpl implements FileService {

    private final FileStorageProperties fileStorageProperties;
    private final FileHashRepository repository;
    private Path fileStorageLocation;

    @PostConstruct
    public void init() {

        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            String message = "Could not create the directory where the uploaded files will be stored";
            log.error(message, ex);
            throw new FileStorageException(message, ex);
        }
    }

    @Override
    public void save(MultipartFile file) {
        String hashMd5 = null;
        String hashSha = null;
        HashFunction md5 = Hashing.md5();
        HashFunction sha256 = Hashing.sha256();
        try {
            hashMd5 = calculateHash(file, md5);
            hashSha = calculateHash(file, sha256);

        } catch (IOException e) {
            log.error("Could not calculate hash", e);
            throw new FileStorageException("Could not calculate hash");
        }
        FileHash fileHash = FileHash.builder()
                .fileName(file.getOriginalFilename())
                .md5(hashMd5)
                .sha256(hashSha)
                .build();
        if (checkIfAlreadyExist(hashMd5, hashSha)) {
            storeFile(file);
            repository.save(fileHash);
        }
    }

    @Override
    public List<FileHash> findByHash(String hash) {
        FileHash fileHash = new FileHash();
        if(checkIfAlreadyExist(hash, hash)){
            throw new FileNotFoundException();
        }
        return repository.findByMd5OrSha256(hash, hash);
    }

    @Transactional
    public List<FileHash> deleteByHash(String hash) {
        if(checkIfAlreadyExist(hash, hash)){
            throw new FileNotFoundException();
        }
        return repository.deleteByMd5OrSha256(hash, hash);
    }

    private String calculateHash(MultipartFile multipartFile, HashFunction hashing) throws IOException {
        HashCode hashCode = com.google.common.io.Files.hash(createTempFile(multipartFile), hashing);
        log.info("Calculated hash is :{}",hashCode.toString());
        return hashCode.toString();
    }

    private boolean checkIfAlreadyExist(String md5, String sha256) {
        List<FileHash> result = repository.findByMd5OrSha256(md5, sha256);
        return result.size() <= 0;
    }

    private File createTempFile(MultipartFile multipartFile) throws IOException {
        String fileName = UUID.randomUUID().toString();
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
        }
        return file;
    }

    private String storeFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        try {
            if (fileName.contains("..")) {
                throw new FileStorageException("Filename contains invalid path sequence " + fileName);
            }
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
}
