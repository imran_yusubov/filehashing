package ru.file.hash.service;

import org.springframework.web.multipart.MultipartFile;
import ru.file.hash.model.FileHash;
import java.util.List;

public interface FileService {

    void save(MultipartFile file);

    List<FileHash> findByHash(String hash);
}
