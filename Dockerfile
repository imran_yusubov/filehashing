FROM openjdk:8-jdk-alpine
ADD build/libs/file-hashing-1.0-SNAPSHOT.jar file-hashing-1.0-SNAPSHOT.jar
MAINTAINER ilkin.ismailov@yahoo.com
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/file-hashing-1.0-SNAPSHOT.jar"]
EXPOSE 8085
